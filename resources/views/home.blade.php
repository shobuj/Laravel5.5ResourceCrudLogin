@extends('layouts.app')

@section('content')
<div class="container">

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
     @endif


    <div class="row">
        <a href="{{ url('/tickets/create') }}" class="btn btn-info">Create Ticket</a>
        <a href="{{ url('/tickets') }}" class="btn btn-primary">All Ticket</a>
    </div>
</div>
@endsection
