
@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                     @endforeach
                </ul>
            </div> <br />
         @endif



                    <div class="row">
            {{--<form action="{{ url('/tickets/create') }}" method="post">--}}
            <form action="{{ route('tickets.store') }}" method="post">
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label for="title">Title Name</label>
                    <input type="text" name="title" id="title" class="form-control" placeholder="Enter title..">
                </div>
                <div class="form-group">
                    <label for="description">Description Name</label>
                    <textarea type="text" name="description" id="description" class="form-control" placeholder="Enter description..">
                </textarea></div>
                <button type="submit" class="">Create Ticket</button>
            </form>
        </div>




    </div>
@endsection