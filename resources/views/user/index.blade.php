@extends('layouts.app')
@section('content')

    <div class="container">
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
         @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{ Session::get('danger') }}
            </div>
        @endif
        <div class="row">

            <a href="{{ action('HomeController@index') }}" class="btn btn-primary pull-right">Back Home</a>

            <br>
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            @foreach($tickets as $ticket)
                <tr>
                    <td>{{ $ticket->id }}</td>
                    <td>{{ $ticket->title }}</td>
                    <td>{{ $ticket->description }}</td>
                    <td>
                        <a href="{{ action('TicketController@show', $ticket->id) }}" class="btn btn-info">Show</a>
                        <a href="{{ action('TicketController@edit', $ticket->id) }}" class="btn btn-primary">Edit</a>

                        <form method="post" action="{{ action('TicketController@destroy', $ticket->id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </table>

        </div>
    </div>

@endsection