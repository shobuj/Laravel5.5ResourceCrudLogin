@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="form group pull-right">
            <a href="{{ action('TicketController@index') }}" class="btn btn-primary">Back Home</a>
        </div>
        <br>
        <div class="row">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>

                </tr>
                <tr>
                    <td>{{ $ticket->id }}</td>
                    <td>{{ $ticket->title }}</td>
                    <td>{{ $ticket->description }}</td>
                </tr>
            </table>
        </div>
    </div>


@endsection