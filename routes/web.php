<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('tickets', 'TicketController');
//
//Route::get('/tickets', 'TicketController@index');
//
//Route::get('/tickets/create', 'TicketController@create');
//Route::post('/tickets/create', 'TicketController@store');
//
//Route::get('/tickets/{id}', 'TicketController@show');
//
//Route::get('/tickets/edit/{id}', 'TicketController@edit');
//Route::patch('/tickets/edit/{id}', 'TicketController@update');
//
//
//Route::delete('/tickets/{id}', 'TicketController@destroy');
